
function varargout = XMCD_STXM_GUI_PolLux(varargin)
    % XMCD_STXM_GUI_POLLUX MATLAB code for XMCD_STXM_GUI_PolLux.fig
    %      XMCD_STXM_GUI_POLLUX, by itself, creates a new XMCD_STXM_GUI_POLLUX or raises the existing
    %      singleton*.
    %
    %      H = XMCD_STXM_GUI_POLLUX returns the handle to a new XMCD_STXM_GUI_POLLUX or the handle to
    %      the existing singleton*.-
    %
    %      XMCD_STXM_GUI_POLLUX('C4*piALLBACK',hObject,eventData,handles,...) calls the local
    %      function named CALLBACK in XMCD_STXM_GUI_POLLUX.M with the given input arguments.
    %
    %      XMCD_STXM_GUI_POLLUX('Property','Value',...) creates a new XMCD_STXM_GUI_POLLUX or raises the
    %      existing singleton*.  Starting from the left, property value pairs are
    %      applied to the GUI before XMCD_STXM_GUI_PolLux_OpeningFcn gets called.  An
    %      unrecognized property name or invalid value makes property application
    %      stop.  All inputs are passed to XM CD_STXM_GUI_PolLux_OpeningFcn via varargin.
    %
    %      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
    %      instance to run (singleton)".
    %
    % See also: GUIDE, GUIDATA, GUIHANDLES

    % Edit the above text to modify the response to help XMCD_STXM_GUI_PolLux

    % Last Modified by GUIDE v2.5 18-May-2021 14:12:08

    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @XMCD_STXM_GUI_PolLux_OpeningFcn, ...
                       'gui_OutputFcn',  @XMCD_STXM_GUI_PolLux_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT


% --- Executes just before XMCD_STXM_GUI_PolLux is made visible.
function XMCD_STXM_GUI_PolLux_OpeningFcn(hObject, eventdata, handles, varargin)
    % This function has no output args, see OutputFcn.
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    % varargin   command line arguments to XMCD_STXM_GUI_PolLux (see VARARGIN)

    % Choose default command line output for XMCD_STXM_GUI_PolLux
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);

    setappdata(0, 'stxm_gui',gcf);

    stxm_gui = getappdata(0,'stxm_gui');

    % Shows a blank image in the areas

    I_0 = ones(512,512);

    hAxis = findobj(stxm_gui,'type','axes');
    setappdata(stxm_gui, 'positive_image', I_0);    
    setappdata(stxm_gui, 'negative_image', I_0);    

    for i=1:3  
        axes(hAxis(i));
        imshow(I_0);
    end

    % Setting up the drift correction sliders
    set(handles.drift_x_slider,'Value',0);
    set(handles.drift_x_edit,'String','0');
    setappdata(stxm_gui, 'drift_x', 0);

    set(handles.drift_y_slider,'Value',0);
    set(handles.drift_y_edit,'String','0');
    setappdata(stxm_gui, 'drift_y', 0);

    set(handles.magnification_slider,'Value',1);
    set(handles.magnification_slider,'Min',1);
    set(handles.magnification_edit,'String','1');
    setappdata(stxm_gui, 'magnification', 1);

    setappdata(stxm_gui, 'cropOuterEdge', 0);

    % Setting up the data handling part
    set(handles.date_edit,'String',datestr(now,29));
    setappdata(stxm_gui, 'today',datestr(now,29));
    clear today

    set(handles.first_img_edit,'String','001');
    setappdata(stxm_gui, 'firstImageNo','001');
    set(handles.second_img_edit,'String','002');
    setappdata(stxm_gui, 'secondImageNo','002');

    setappdata(stxm_gui, 'position_correction', 0);

    setappdata(stxm_gui,'imageRootPath','Z:\Data1\');

    setappdata(stxm_gui,'key_binding',0);

    setappdata(stxm_gui,'normalize',0);
    setappdata(stxm_gui,'normalize_images',0);
    
    setappdata(stxm_gui, 'OD', 0);
    
    setappdata(stxm_gui, 'autoPolarization', 1);

    % UIWAIT makes XMCD_STXM_GUI_PolLux wait for user response (see UIRESUME)
    % uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = XMCD_STXM_GUI_PolLux_OutputFcn(hObject, eventdata, handles) 
    % varargout  cell array for returning output args (see VARARGOUT);
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Get default command line output from handles structure
    varargout{1} = handles.output;


% --- Executes on slider movement.
function drift_x_slider_Callback(hObject, eventdata, handles)
    % hObject    handle to drift_x_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

    stxm_gui = getappdata(0,'stxm_gui');

    setappdata(stxm_gui, 'drift_x', get(hObject,'Value'));
    set(handles.drift_x_edit,'String',num2str(get(hObject,'Value')));

    recalculate_xmcd(handles);



% --- Executes during object creation, after setting all properties.
function drift_x_slider_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to drift_x_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called

    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end



function drift_x_edit_Callback(hObject, eventdata, handles)
    % hObject    handle to drift_x_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hints: get(hObject,'String') returns contents of drift_x_edit as text
    %        str2double(get(hObject,'String')) returns contents of drift_x_edit as a double

    stxm_gui = getappdata(0,'stxm_gui');

    setappdata(stxm_gui, 'drift_x', floor(str2num(get(hObject,'String'))));
    set(handles.drift_x_slider,'Value',getappdata(stxm_gui, 'drift_x'));

    recalculate_xmcd(handles);

% --- Executes during object creation, after setting all properties.
function drift_x_edit_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to drift_x_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called

    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


% --- Executes on slider movement.
function drift_y_slider_Callback(hObject, eventdata, handles)
    % hObject    handle to drift_y_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

    stxm_gui = getappdata(0,'stxm_gui');

    setappdata(stxm_gui, 'drift_y', get(hObject,'Value'));
    set(handles.drift_y_edit,'String',num2str(get(hObject,'Value')));

    recalculate_xmcd(handles);


% --- Executes during object creation, after setting all properties.
function drift_y_slider_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to drift_y_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called

    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end


% --- Executes on button press in load_image_button.
function load_image_button_Callback(hObject, eventdata, handles)
    % hObject    handle to load_image_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    plot_direct_images(handles)

% --- Executes on button press in save_image_button.
function save_image_button_Callback(hObject, eventdata, handles)
    % hObject    handle to save_image_button (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    fprintf('XMCD image will be saved\n');
    save_xmcd_image(handles);


function date_edit_Callback(hObject, eventdata, handles)
    % hObject    handle to date_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hints: get(hObject,'String') returns contents of date_edit as text
    %        str2double(get(hObject,'String')) returns contents of date_edit as a double
    stxm_gui = getappdata(0,'stxm_gui');

    setappdata(stxm_gui, 'today', get(hObject,'String'));


% --- Executes during object creation, after setting all properties.
function date_edit_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to date_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called

    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end



function first_img_edit_Callback(hObject, eventdata, handles)
    % hObject    handle to first_img_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hints: get(hObject,'String') returns contents of first_img_edit as text
    %        str2double(get(hObject,'String')) returns contents of first_img_edit as a double
    stxm_gui = getappdata(0,'stxm_gui');

    setappdata(stxm_gui, 'firstImageNo', get(hObject,'String'));

% --- Executes during object creation, after setting all properties.
function first_img_edit_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to first_img_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called

    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end



function second_img_edit_Callback(hObject, eventdata, handles)
    % hObject    handle to second_img_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hints: get(hObject,'String') returns contents of second_img_edit as text
    %        str2double(get(hObject,'String')) returns contents of second_img_edit as a double
    stxm_gui = getappdata(0,'stxm_gui');

    setappdata(stxm_gui, 'secondImageNo', get(hObject,'String'));

% --- Executes during object creation, after setting all properties.
function second_img_edit_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to second_img_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called

    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end



function drift_y_edit_Callback(hObject, eventdata, handles)
    % hObject    handle to drift_y_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hints: get(hObject,'String') returns contents of drift_y_edit as text
    %        str2double(get(hObject,'String')) returns contents of drift_y_edit as a double
    stxm_gui = getappdata(0,'stxm_gui');

    setappdata(stxm_gui, 'drift_y', floor(str2num(get(hObject,'String'))));
    set(handles.drift_y_slider,'Value',getappdata(stxm_gui, 'drift_y'));

    recalculate_xmcd(handles);


% --- Executes during object creation, after setting all properties.
function drift_y_edit_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to drift_y_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called

    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end



function magnification_edit_Callback(hObject, eventdata, handles)
    % hObject    handle to magnification_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hints: get(hObject,'String') returns contents of magnification_edit as text
    %        str2double(get(hObject,'String')) returns contents of magnification_edit as a double
    stxm_gui = getappdata(0,'stxm_gui');

    read_magnification = floor(str2num(get(hObject,'String')));
    if read_magnification > 100
        read_magnification = 100;
        fprintf('WARNING: Magnification cannot be more than 100x');
        set(handles.magnification_edit, 'String', '11');
    end
    if read_magnification < 1
        read_magnification = 1;
        fprintf('WARNING: Magnification cannot be less than 1x');
        set(handles.magnification_edit, 'String', '1');
    end

    setappdata(stxm_gui, 'magnification', read_magnification);
    set(handles.magnification_slider,'Value',getappdata(stxm_gui, 'magnification'));

    recalculate_xmcd(handles);

% --- Executes during object creation, after setting all properties.
function magnification_edit_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to magnification_edit (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called

    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


% --- Executes on slider movement.
function magnification_slider_Callback(hObject, eventdata, handles)
    % hObject    handle to magnification_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hints: get(hObject,'Value') returns position of slider
    %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
    stxm_gui = getappdata(0,'stxm_gui');

    setappdata(stxm_gui, 'magnification', get(hObject,'Value'));
    set(handles.magnification_edit,'String',num2str(get(hObject,'Value')));

    recalculate_xmcd(handles);

% --- Executes during object creation, after setting all properties.
function magnification_slider_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to magnification_slider (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called

    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end

% --- Executes on selection change in detector_selection.
function detector_selection_Callback(hObject, eventdata, handles)
    % hObject    handle to detector_selection (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hints: contents = cellstr(get(hObject,'String')) returns detector_selection contents as cell array
    %        contents{get(hObject,'Value')} returns selected item from detector_selection
    stxm_gui = getappdata(0,'stxm_gui');

    setappdata(stxm_gui,'selected_detector',get(hObject,'Value')-1);

% --- Executes during object creation, after setting all properties.
function detector_selection_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to detector_selection (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called

    % Hint: popupmenu controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
    % hObject    handle to checkbox1 (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hint: get(hObject,'Value') returns toggle state of checkbox1

    stxm_gui = getappdata(0,'stxm_gui');   

    setappdata(stxm_gui, 'position_correction', get(hObject,'Value'));

    plot_direct_images(handles);


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
    % hObject    handle to checkbox2 (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hint: get(hObject,'Value') returns toggle state of checkbox2

    stxm_gui = getappdata(0,'stxm_gui');   

    setappdata(stxm_gui, 'cropOuterEdge', get(hObject,'Value'));


    plot_direct_images(handles);


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
    % hObject    handle to pushbutton3 (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    stxm_gui = getappdata(0,'stxm_gui');   

    firstImageNo = getappdata(stxm_gui,'firstImageNo');
    secondImageNo = getappdata(stxm_gui,'secondImageNo');

    firstImageNumber = str2num(firstImageNo)+2;
    secondImageNumber = str2num(secondImageNo)+2;

    %firstImageNumber = str2num(firstImageNo)+3;
    %secondImageNumber = str2num(secondImageNo)+3;

    if firstImageNumber < 10
        firstImageNo = strcat('00',num2str(firstImageNumber));
    elseif firstImageNumber < 100
        firstImageNo = strcat('0',num2str(firstImageNumber));
    else
        firstImageNo = num2str(firstImageNumber);
    end

    if secondImageNumber < 10
        secondImageNo = strcat('00',num2str(secondImageNumber));
    elseif secondImageNumber < 100
        secondImageNo = strcat('0',num2str(secondImageNumber));
    else
        secondImageNo = num2str(secondImageNumber);
    end

    setappdata(stxm_gui,'firstImageNo',firstImageNo);
    setappdata(stxm_gui,'secondImageNo',secondImageNo);

    set(handles.first_img_edit,'string',firstImageNo);
    set(handles.second_img_edit,'string',secondImageNo);

    plot_direct_images(handles);


% --- Executes on key press with focus on figure1 or any of its controls.
function figure1_WindowKeyPressFcn(hObject, eventdata, handles)
    % hObject    handle to figure1 (see GCBO)
    % eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
    %	Key: name of the key that was pressed, in lower case
    %	Character: character interpretation of the key(s) that was pressed
    %	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
    % handles    structure with handles and user data (see GUIDATA)

    stxm_gui = getappdata(0,'stxm_gui');

    if getappdata(stxm_gui,'key_binding');

        switch eventdata.Key
            case 'n'
                pushbutton3_Callback(hObject, eventdata, handles)
            case 'b'
                pushbutton4_Callback(hObject, eventdata, handles)
            case 'l'
                load_image_button_Callback(hObject, eventdata, handles)
            case 's'
                save_image_button_Callback(hObject, eventdata, handles)
            case 'uparrow'
                setappdata(stxm_gui, 'drift_y',getappdata(stxm_gui, 'drift_y')+1);
                set(handles.drift_y_edit,'String',getappdata(stxm_gui, 'drift_y'));
                recalculate_xmcd(handles);
            case 'downarrow'
                setappdata(stxm_gui, 'drift_y',getappdata(stxm_gui, 'drift_y')-1);
                set(handles.drift_y_edit,'String',getappdata(stxm_gui, 'drift_y'));
                recalculate_xmcd(handles);        
            case 'leftarrow'
                setappdata(stxm_gui, 'drift_x',getappdata(stxm_gui, 'drift_x')-1);
                set(handles.drift_x_edit,'String',getappdata(stxm_gui, 'drift_x'));
                recalculate_xmcd(handles);        
            case 'rightarrow'
                setappdata(stxm_gui, 'drift_x',getappdata(stxm_gui, 'drift_x')+1);
                set(handles.drift_x_edit,'String',getappdata(stxm_gui, 'drift_x'));
                recalculate_xmcd(handles); 

        end

    end


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
    % hObject    handle to checkbox3 (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hint: get(hObject,'Value') returns toggle state of checkbox3

    stxm_gui = getappdata(0,'stxm_gui');
    setappdata(stxm_gui,'key_binding',get(hObject,'Value'));


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
    % hObject    handle to pushbutton4 (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    stxm_gui = getappdata(0,'stxm_gui');   

    firstImageNo = str2num(getappdata(stxm_gui,'firstImageNo'));
    secondImageNo = str2num(getappdata(stxm_gui,'secondImageNo'));

    if (firstImageNo <= 2) | (secondImageNo <= 2)
        firstImageNumber = firstImageNo;
        secondImageNumber = secondImageNo;
    else
        firstImageNumber = firstImageNo-2;
        secondImageNumber = secondImageNo-2;
    end

    %firstImageNumber = str2num(firstImageNo)+3;
    %secondImageNumber = str2num(secondImageNo)+3;

    if firstImageNumber < 10
        firstImageNo = strcat('00',num2str(firstImageNumber));
    elseif firstImageNumber < 100
        firstImageNo = strcat('0',num2str(firstImageNumber));
    else
        firstImageNo = num2str(firstImageNumber);
    end

    if secondImageNumber < 10
        secondImageNo = strcat('00',num2str(secondImageNumber));
    elseif secondImageNumber < 100
        secondImageNo = strcat('0',num2str(secondImageNumber));
    else
        secondImageNo = num2str(secondImageNumber);
    end

    setappdata(stxm_gui,'firstImageNo',firstImageNo);
    setappdata(stxm_gui,'secondImageNo',secondImageNo);

    set(handles.first_img_edit,'string',firstImageNo);
    set(handles.second_img_edit,'string',secondImageNo);

    plot_direct_images(handles);


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
    % hObject    handle to checkbox4 (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hint: get(hObject,'Value') returns toggle state of checkbox4
    stxm_gui = getappdata(0,'stxm_gui');
    setappdata(stxm_gui,'normalize',get(hObject,'Value'));


% --- Executes on button press in checkbox5.
function checkbox5_Callback(hObject, eventdata, handles)
    % hObject    handle to checkbox5 (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hint: get(hObject,'Value') returns toggle state of checkbox5
    stxm_gui = getappdata(0,'stxm_gui');
    setappdata(stxm_gui,'normalize_images',get(hObject,'Value'));
    plot_direct_images(handles); 

% --- Executes on button press in checkbox6.
function checkbox6_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox6
    stxm_gui = getappdata(0, 'stxm_gui');
    setappdata(stxm_gui, 'OD', get(hObject,'Value'));
    plot_direct_images(handles);


% --- Executes on button press in autoPolarizationCB.
function autoPolarizationCB_Callback(hObject, eventdata, handles)
% hObject    handle to autoPolarizationCB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of autoPolarizationCB
    stxm_gui = getappdata(0, 'stxm_gui');
    setappdata(stxm_gui, 'autoPolarization', get(hObject,'Value'));
    plot_direct_images(handles);
    
% Additional functions

function plot_direct_images(handles)

    print_message(handles, '');
    stxm_gui = getappdata(0,'stxm_gui');
    imageRootPath = getappdata(stxm_gui,'imageRootPath');
    today = getappdata(stxm_gui,'today');
    firstImageNo = getappdata(stxm_gui,'firstImageNo');
    secondImageNo = getappdata(stxm_gui,'secondImageNo');

    firstImagePath = strcat(imageRootPath,today,'\','Sample_Image_',today,'_',firstImageNo,'.hdf5')
    secondImagePath = strcat(imageRootPath,today,'\','Sample_Image_',today,'_',secondImageNo,'.hdf5')

    if (not(exist(firstImagePath)) | not(exist(secondImagePath)))
       fprintf('One - or both - of the images do not exist. Did you synchronize??\n'); 
       axes(handles.xmcd_img);
       imshow(ones(32));
       axes(handles.positive_img);
       imshow(ones(32));
       axes(handles.negative_img);
       imshow(ones(32));
       print_message(handles, 'WARNING: One - or both - of the images do not exist. Did you synchronize?');
       return
    end

    [I_pos, I_neg] = loadhdf5Image(handles, firstImagePath, secondImagePath);
    setappdata(stxm_gui, 'positive_image',I_pos);
    setappdata(stxm_gui, 'negative_image',I_neg);

    % Plots the two helicities
    axes(handles.negative_img); % NEGATIVE Helicity
    I_neg = I_neg - min(min(I_neg));
    I_neg = imrotate(I_neg,90);
    imshow(I_neg./max(max(I_neg)));
    axes(handles.positive_img); % POSITIVE Helicity
    I_pos = I_pos - min(min(I_pos));
    I_pos = imrotate(I_pos,90);
    imshow(I_pos./max(max(I_pos)));

    recalculate_xmcd(handles);

function [I_pos, I_neg] = loadhdf5Image(handles, firstImagePathComplete, secondImagePathComplete)

    stxm_gui = getappdata(0, 'stxm_gui');

    detector = getappdata(stxm_gui, 'selected_detector');
    autoPolarization = getappdata(stxm_gui, 'autoPolarization');

    if detector == 0
        print_message(handles, 'ERROR: Please select a detector');
        error('Please select a detector');
    end

    % Here we get the version of Matlab (the command changes between versions)
    currVersion = version('-release');

    if str2double(currVersion(1:4)) >= 2009
        command = 'h5';
    else
        command = 'hdf5';
    end

    % Checks that the helicities are correctly selected
    % First we try with the orbit bumps

    if autoPolarization

        dataset = strcat('/entry1/collection/ring_y_asym/value'); % This looks at the asymmetry of the y position of the beam
        firstHelicity = eval(strcat(command,'read(firstImagePathComplete,dataset)'));
        secondHelicity = eval(strcat(command,'read(secondImagePathComplete,dataset)'));%-1*firstHelicity;

        % Here we look at which image has the positive/negative helicity
        if firstHelicity == secondHelicity
           dataset = strcat('/entry1/collection/Girder_y/value'); % This looks at the girders
           firstHelicity = eval(strcat(command,'read(firstImagePathComplete,dataset)'));
           secondHelicity = eval(strcat(command,'read(secondImagePathComplete,dataset)'));%-1*firstHelicity;
        end

        if firstHelicity > secondHelicity
            xmcdOrder = {'first', 'second'};
        elseif secondHelicity > firstHelicity
            xmcdOrder = {'second', 'first'};
        else % if both helicities are the same
        % xmcdOrder = {'second', 'first'};
            print_message(handles,'ERROR: Both helicities are the same (untick AutoPolarization)');
            error('Both helicities are the same');
        end

        % Here we look if one of the two images has linear light
        if firstHelicity == 0 | secondHelicity == 0
            fprintf('Warning: One of the two images was done at linear polarization\n');
            print_message(handles,'Warning: One of the two images was done at linear polarization');
        end

    else
         xmcdOrder = {'first', 'second'};
    end

    %% READ IMAGE

    % Here we define the dataset
    for i=1:1 % Horrible trick to make the break function work in the switch
        switch detector
            case 1
                detectorPath = '/analog0/';
                break;
            case 2
                detectorPath = '/counter0/';
                break;
            case 3
                detectorPath = '/phc/';
                break;
            case 4
                detectorPath = '/zmq/';
                break;
        end
    end

    dataset = strcat('/entry1',detectorPath,'data');

    try % Checks that everything is working fine
        temp = eval(strcat(command,'info(firstImagePathComplete,dataset)')); 
        temp = eval(strcat(command,'info(secondImagePathComplete,dataset)')); 
    catch
        error('Not a suitable detector selected');
    end

    clear temp

    % Reads the images
    first_string = char(strcat(command,'read(',char(xmcdOrder(1)),'ImagePathComplete,dataset)'));
    second_string = char(strcat(command,'read(',char(xmcdOrder(2)),'ImagePathComplete,dataset)'));

    I_pos = double(eval(first_string));
    I_neg = double(eval(second_string));

    % Correct for the beam position (normalize images)

    normalize_images = getappdata(stxm_gui, 'normalize_images');

    if normalize_images
        I_pos_norm = ones(size(I_pos));
        I_neg_norm = ones(size(I_neg));

        try
            dataset = strcat('/entry1/control/data');
            I_pos_norm = double(eval(first_string));
            I_neg_norm = double(eval(second_string));

        catch
            fprintf('WARNING - Problems with normalization - defaulting to NO normalization');
            print_message(handles, 'WARNING - Problems with normalization - defaulting to NO normalization');
            I_pos_norm = ones(size(I_pos));
            I_neg_norm = ones(size(I_neg));
        end

        I_pos = I_pos./I_pos_norm;
        I_neg = I_neg./I_neg_norm;

        %I_pos = I_pos_norm;
        %I_neg = I_neg_norm;

    end

    % Gets the grid
    dataset_x = strcat('/entry1/instrument/sample_x/data');
    dataset_y = strcat('/entry1/instrument/sample_y/data');
    dataset_setpx = strcat('/entry1',detectorPath,'sample_x');
    dataset_setpy = strcat('/entry1',detectorPath,'sample_y');

    x_points = double(eval(strcat(command,'read(',char(xmcdOrder(1)),'ImagePathComplete,dataset_x)')));
    y_points = double(eval(strcat(command,'read(',char(xmcdOrder(1)),'ImagePathComplete,dataset_y)')));
    setpx = double(eval(strcat(command,'read(',char(xmcdOrder(1)),'ImagePathComplete,dataset_setpx)')));
    setpy = double(eval(strcat(command,'read(',char(xmcdOrder(1)),'ImagePathComplete,dataset_setpy)')));

    setappdata(stxm_gui,'x_points_1',x_points);
    setappdata(stxm_gui,'y_points_1',y_points);
    setappdata(stxm_gui,'setp_x_1',setpx);
    setappdata(stxm_gui,'setp_y_1',setpy);

    x_points = double(eval(strcat(command,'read(',char(xmcdOrder(2)),'ImagePathComplete,dataset_x)')));
    y_points = double(eval(strcat(command,'read(',char(xmcdOrder(2)),'ImagePathComplete,dataset_y)')));
    setpx = double(eval(strcat(command,'read(',char(xmcdOrder(2)),'ImagePathComplete,dataset_setpx)')));
    setpy = double(eval(strcat(command,'read(',char(xmcdOrder(2)),'ImagePathComplete,dataset_setpy)')));

    setappdata(stxm_gui,'x_points_2',x_points);
    setappdata(stxm_gui,'y_points_2',y_points);
    setappdata(stxm_gui,'setp_x_2',setpx);
    setappdata(stxm_gui,'setp_y_2',setpy);
    
    %% Doing the position correction
    
    correct_data = getappdata(stxm_gui, 'position_correction');
    
    if correct_data
        x_points = getappdata(stxm_gui,'x_points_1');
        y_points = getappdata(stxm_gui,'y_points_1');
        setpx = getappdata(stxm_gui,'setp_x_1');
        setpy = getappdata(stxm_gui,'setp_y_1');

        I_pos = reshapeImage(x_points, y_points, I_pos, setpx, setpy);

        x_points = getappdata(stxm_gui,'x_points_2');
        y_points = getappdata(stxm_gui,'y_points_2');
        setpx = getappdata(stxm_gui,'setp_x_2');
        setpy = getappdata(stxm_gui,'setp_y_2');

        I_neg = reshapeImage(x_points, y_points, I_neg, setpx, setpy);  
        
        I_pos = imrotate(I_pos,-90);
        I_neg = imrotate(I_neg,-90);
    end

    return


function recalculate_xmcd(handles)

    stxm_gui = getappdata(0,'stxm_gui');
    
    try

        % Gets the images and the drift values
        drift_x = getappdata(stxm_gui,'drift_x');
        drift_y = getappdata(stxm_gui,'drift_y');
        I_pos = getappdata(stxm_gui,'positive_image');
        I_neg = getappdata(stxm_gui,'negative_image');
        n_int = getappdata(stxm_gui,'magnification');
        cropOuterEdge = getappdata(stxm_gui, 'cropOuterEdge');
        

        I_pos = imrotate(I_pos,90);
        I_neg = imrotate(I_neg,90);
        
        I_reg = zeros(size(I_pos)); 

        if n_int == 1
            I_reg = circshift(I_neg,[-drift_y -drift_x]);
        else
            dx = drift_x;
            dy = drift_y;
            drift_x = dy / n_int;
            drift_y = dx / n_int;

            for j=1:size(I_neg,1)
                for k=1:size(I_neg,2)

                    j_cur = j+floor(drift_x); % ROW --> A(row, col)
                    k_cur = k+floor(drift_y); % COLUMN

                    if (j_cur < 1 || j_cur+1 > size(I_neg,1)) || (k_cur < 1 || k_cur+1 > size(I_neg,2))
                        I_reg(j,k) = I_neg(1,1);
                    else
                        int_1 = I_neg(j_cur,k_cur) + (I_neg(j_cur+1,k_cur)-I_neg(j_cur,k_cur))*(drift_x-floor(drift_x));
                        int_2 = I_neg(j_cur,k_cur+1) + (I_neg(j_cur+1,k_cur+1)-I_neg(j_cur,k_cur+1))*(drift_x-floor(drift_x));
                        int_3 = int_1 + (int_2 - int_1)*(drift_y-floor(drift_y));
                        I_reg(j,k) = int_3;
                    end
                end
            end

            clear dx dy
        end

        I_neg = I_reg;

        drift_x = getappdata(stxm_gui,'drift_x')/(n_int)^2;
        drift_y = getappdata(stxm_gui,'drift_y')/(n_int)^2;

        OD = getappdata(stxm_gui,'OD');

        if OD
            I_xmcd = (log(I_pos)-log(I_neg))./(log(I_pos) + log(I_neg));
            I_sum = log(I_pos) + log(I_neg);
        else
            I_xmcd = (I_pos - I_neg)./(I_pos + I_neg);
            I_sum = I_pos + I_neg;
        end
        setappdata(stxm_gui, 'xmcd_image', I_xmcd);
        setappdata(stxm_gui,'sum_image',I_sum);
        I_xmcd = I_xmcd - min(min(I_xmcd(abs(ceil(drift_y))+2:end-(abs(ceil(drift_y))+2),abs(ceil(drift_x))+2:end-(abs(ceil(drift_x))+2))));
        I_xmcd = I_xmcd./max(max(I_xmcd(abs(ceil(drift_y))+2:end-(abs(ceil(drift_y))+2),abs(ceil(drift_x))+2:end-(abs(ceil(drift_x))+2))));
        axes(handles.xmcd_img); % XMCD_image
        imshow(I_xmcd)
    catch ME
        print_message(handles, strcat('ERROR: Processing of asymmetry image -- ',ME.identifier));
        axes(handles.xmcd_img);
        imshow(ones(size(I_pos)));
        rethrow(ME);
    end


function save_xmcd_image(handles)

    stxm_gui = getappdata(0,'stxm_gui');   

    imageRootPath = getappdata(stxm_gui,'imageRootPath');
    today = getappdata(stxm_gui,'today');
    firstImageNo = getappdata(stxm_gui, 'firstImageNo');
    I_xmcd = getappdata(stxm_gui,'xmcd_image');
    I_sum = getappdata(stxm_gui,'sum_image');

    drift_x = getappdata(stxm_gui,'drift_x');
    drift_y = getappdata(stxm_gui,'drift_y');
    n_int = getappdata(stxm_gui,'magnification');

    normImages = getappdata(stxm_gui, 'normalize');

    savePath = strcat(imageRootPath,today,'\Analysed\');
    % Creates the save folder if it does not exist
    if ~exist(savePath,'dir')
        mkdir(savePath);
        fprintf('Warning: Save folder did not exist - it was now created for you\n');
    end
    % Normalization of the image between 0 and 1

    if normImages

        drift_x = drift_x/n_int;
        drift_y = drift_y/n_int;

        I_xmcd = I_xmcd-min(min(I_xmcd(abs(ceil(drift_y))+1:end-(abs(ceil(drift_y))+1),abs(ceil(drift_x))+1:end-(abs(ceil(drift_x))+1))));
        I_xmcd = I_xmcd./max(max(I_xmcd(abs(ceil(drift_y))+1:end-(abs(ceil(drift_y))+1),abs(ceil(drift_x))+1:end-(abs(ceil(drift_x))+1))));

        I_sum = I_sum-min(min(I_sum(abs(ceil(drift_y))+1:end-(abs(ceil(drift_y))+1),abs(ceil(drift_x))+1:end-(abs(ceil(drift_x))+1))));
        I_sum = I_sum./max(max(I_sum(abs(ceil(drift_y))+1:end-(abs(ceil(drift_y))+1),abs(ceil(drift_x))+1:end-(abs(ceil(drift_x))+1))));

    end
    % Saving the image
    t = Tiff(strcat(savePath,'XMCD_Image_',today,'_',firstImageNo,'.tif'),'w');
    tagstruct.ImageLength = size(I_xmcd, 1); 
    tagstruct.ImageWidth = size(I_xmcd, 2); 
    tagstruct.Compression = Tiff.Compression.None; 
    tagstruct.SampleFormat = Tiff.SampleFormat.IEEEFP; 
    tagstruct.Photometric = Tiff.Photometric.MinIsBlack; 
    tagstruct.BitsPerSample = 32; % 32; 
    tagstruct.SamplesPerPixel = 1; % 1; 
    tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky; 
    t.setTag(tagstruct); 
    t.write(single(I_xmcd));   
    t.close();

    t = Tiff(strcat(savePath,'Sum_Image_',today,'_',firstImageNo,'.tif'),'w');
    tagstruct.ImageLength = size(I_xmcd, 1); 
    tagstruct.ImageWidth = size(I_xmcd, 2); 
    tagstruct.Compression = Tiff.Compression.None; 
    tagstruct.SampleFormat = Tiff.SampleFormat.IEEEFP; 
    tagstruct.Photometric = Tiff.Photometric.MinIsBlack; 
    tagstruct.BitsPerSample = 32; % 32; 
    tagstruct.SamplesPerPixel = 1; % 1; 
    tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky; 
    t.setTag(tagstruct); 
    t.write(single(I_sum));   
    t.close();
    
    print_message(handles, 'Asymmetry image saved');


function I_reg = reshapeImage(x_points, y_points, V, setpx, setpy, drift_x, drift_y, n_int)
    
    if nargin == 5
       drift_x = 0;
       drift_y = 0;
       n_int = 1;
    end
    
    stxm_gui = getappdata(0, 'stxm_gui');
    cropOuterEdge = getappdata(stxm_gui, 'cropOuterEdge');
    
    Xq = reshape(x_points, size(V,1), size(V,2));
    Yq = reshape(y_points, size(V,1), size(V,2));
    
    [X,Y] = meshgrid(setpx,setpy);
    
    X = X-mean(mean(X))-drift_x/(n_int*size(V,1));
    Y = Y-mean(mean(Y))+drift_y/(n_int*size(V,2));
    Xq = Xq-mean(mean(Xq));
    Yq = Yq-mean(mean(Yq));
    
    if cropOuterEdge
        V = V(2:end-1,2:end-1);
        Xq = Xq(2:end-1, 2:end-1);
        Yq = Yq(2:end-1, 2:end-1);
        X = X(2:end-1,2:end-1);
        Y = Y(2:end-1,2:end-1);
    end
    
    I_reg = griddata(Xq,Yq,V,X,Y);
    
    I_reg = flipdim(I_reg,1);
    
    
function print_message(handles,message)
    set(handles.messageWindow,'string',message);
